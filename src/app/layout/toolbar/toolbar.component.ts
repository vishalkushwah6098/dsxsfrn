import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
  @Output() emitShowSidebar = new EventEmitter();
  showSidebar = true;
  constructor() { }

  ngOnInit() {
  }

  toggleSidebar() {
    this.showSidebar = !this.showSidebar;
    this.emitShowSidebar.emit(this.showSidebar);
  }
}
