import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  showSidebar: boolean;
  constructor() { }
  toggleSidebar(event: any) {
    this.showSidebar = event;
    // console.log(event)
  }
}
