import { Component, OnInit } from '@angular/core';
import { Lead } from 'src/app/model/lead/lead';
import { LeadDataService } from 'src/app/services/data/lead/lead-data.service';

@Component({
  selector: 'app-lead-detail',
  templateUrl: './lead-detail.component.html',
  styleUrls: ['./lead-detail.component.scss'],
})
export class LeadDetailComponent implements OnInit {

  lead: Lead
  constructor(private leadDataService: LeadDataService) { }

  async ngOnInit() {
    this.lead = await this.leadDataService.getLeadById('');
  }
}
