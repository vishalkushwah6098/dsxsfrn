import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Form, FormBuilder, FormControl } from '@angular/forms';
import { Lead } from '../../model/lead/lead';
import { LeadDataService } from '../../services/data/lead/lead-data.service';

@Component({
  selector: 'app-add-lead',
  templateUrl: './add-lead.page.html',
  styleUrls: ['./add-lead.page.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AddLeadPage implements OnInit {

  lead = new Lead();
  isFormDirty = false;
  savingChanges = false;
  
  addLeadForm = this.fb.group({
    name: new FormControl(''),
    email: new FormControl(''),
    phoneNumber: new FormControl(''),
    alternatePhoneNumber: new FormControl(''),
  })

  constructor(private fb: FormBuilder, private leadDataService: LeadDataService) {
  }

  ngOnInit() {
    console.log('lead-created?')

    if (this.addLeadForm.dirty) {
      this.isFormDirty = true;
    }
  }

  async onSave() {
    this.savingChanges = true
    await this.createLead();
    this.addLeadForm.reset();
    this.savingChanges = false;
  }

  async createLead() {
    this.lead.fields.name = this.addLeadForm.value.name;
    this.lead.fields.phoneNumber = this.addLeadForm.value.phoneNumber;
    this.lead.fields.alternatePhoneNumber = this.addLeadForm.value.alternatePhoneNumber;
    this.lead.creationTimestamp = Date.now();
    this.lead.modificationTimestamp = Date.now();
    await this.leadDataService.createLead(this.lead);
    console.log('lead-created')
  }

}
