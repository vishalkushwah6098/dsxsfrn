import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Lead } from 'src/app/model/lead/lead';
import { LeadDataService } from 'src/app/services/data/lead/lead-data.service';

@Component({
  selector: 'app-edit-lead',
  templateUrl: './edit-lead.page.html',
  styleUrls: ['./edit-lead.page.scss'],
})
export class EditLeadPage implements OnInit {


  lead: Lead;
  isFormDirty = false;
  savingChanges = false;

  editLeadForm = this.fb.group({
    name: new FormControl(''),
    phoneNumber: new FormControl(''),
    alternatePhoneNumber: new FormControl(''),
  })


  constructor(private fb: FormBuilder, private leadDataService: LeadDataService) {
  }

  async ngOnInit() {
    this.lead = await this.leadDataService.getLeadById('');
    console.log(this.lead)
    this.editLeadForm.setValue({
      name: this.lead.fields.name?this.lead.fields.name:'',
      phoneNumber: this.lead.fields.phoneNumber ? this.lead.fields.phoneNumber : '',
      alternatePhoneNumber: this.lead.fields.alternatePhoneNumber ? this.lead.fields.alternatePhoneNumber : '',
    })
    if (this.editLeadForm.dirty) {
      this.isFormDirty = true;
    }
  }

  async onSave() {
    this.savingChanges = true
    await this.updateLead();
    this.editLeadForm.reset();
    this.savingChanges = false;
  }

  async updateLead() {
    this.lead.fields.name = this.editLeadForm.value.name;
    this.lead.fields.phoneNumber = this.editLeadForm.value.phoneNumber;
    this.lead.fields.alternatePhoneNumber = this.editLeadForm.value.alternatePhoneNumber;
    this.lead.modificationTimestamp = Date.now();
    await this.leadDataService.updateLead(this.lead);
  }

}
