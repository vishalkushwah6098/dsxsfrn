import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditLeadPageRoutingModule } from './edit-lead-routing.module';

import { EditLeadPage } from './edit-lead.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EditLeadPageRoutingModule
  ],
  declarations: [EditLeadPage]
})
export class EditLeadPageModule {}
