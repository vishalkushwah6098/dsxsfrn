import { Action } from "../actions/action"
import { LeadDefaultFields } from "./lead-default-fields"

export class Lead {
    id: string
    status: string
    createdBy: string
    creationTimestamp: number
    modificationTimestamp: number
    fields = new LeadDefaultFields('', '', '');
    actions: Action[] = []

    fromJSON(leadObject: any) {
        const lead = new Lead();
        this.id = leadObject.id ? leadObject.id : '';
        this.createdBy = leadObject.createdBy ? leadObject.createdBy : '';
        this.creationTimestamp = leadObject.creationTimestamp ? leadObject.creationTimestamp : 0;
        this.modificationTimestamp = leadObject.modificationTimestamp ? leadObject.modificationTimestamp : 0;
        this.fields = LeadDefaultFields.fromJSON(lead, leadObject);
        return lead;
    }

}