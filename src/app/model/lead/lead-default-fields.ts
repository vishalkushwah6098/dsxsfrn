import { Lead } from "./lead";
export class LeadDefaultFields {
    name: string
    phoneNumber: string
    alternatePhoneNumber: string
    constructor(name: string, phoneNumber: string, alternatePhoneNumber: string) {
        this.name = name ? name : '';
        this.phoneNumber = phoneNumber ? phoneNumber : '';
        this.alternatePhoneNumber = alternatePhoneNumber ? alternatePhoneNumber : '';
    }

    static fromJSON(lead: Lead, leadObject: any) {
        lead.fields.name = leadObject.fields.name ? leadObject.fields.name : '';
        lead.fields.phoneNumber = leadObject.fields.phoneNumber ? leadObject.fields.phoneNumber : '';
        lead.fields.alternatePhoneNumber = leadObject.fields.alternatePhoneNumber ? leadObject.fields.alternatePhoneNumber : '';
        return lead.fields;
    }

}