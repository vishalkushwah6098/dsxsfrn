import { ActionTypes } from "./action-types"

export abstract class Action{
    id: string
    type: ActionTypes
    creationTimestamp: number
    modificationTimestamp: number
}