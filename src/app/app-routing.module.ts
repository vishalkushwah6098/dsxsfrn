import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'add-leads',
    pathMatch: 'full'
  },
  {
    path: 'add-lead', 
    loadChildren: () => import('./lead/add-lead/add-lead.module').then(m => m.AddLeadPageModule)
  },
  {
    path: 'all-leads',
    loadChildren: () => import('./all-leads/all-leads.module').then( m => m.AllLeadsPageModule)
  },
  // {
  //   path: 'edit',
  //   loadChildren: () => import('./lead/edit-lead/edit-lead.module').then(m => m.EditLeadPageModule)
  // },
  // {
  //   path: '**',
  //   loadChildren: () => import('./page-not-found/page-not-found.module').then( m => m.PageNotFoundPageModule)
  // },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
