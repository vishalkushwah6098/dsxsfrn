import { Injectable } from '@angular/core';
import { Lead } from 'src/app/model/lead/lead';

@Injectable({
  providedIn: 'root'
})
export class LeadDataService {

  constructor() { }

  async createLead(lead: Lead) {
    let leads = await this.getAllLeads() as Lead[];
    if (leads) {
      leads.push(lead);
      await localStorage.setItem('leads', JSON.stringify(leads));
    }
    else {
      await localStorage.setItem('leads', JSON.stringify([lead]));
    }
    console.log('lead-added successfully!!')
  }

  async getAllLeads(): Promise<Lead[]> {
    return await JSON.parse(localStorage.getItem('leads'));
  }

  async getLeadById(id: string): Promise<Lead> {
    const leads = await JSON.parse(localStorage.getItem('leads'));
    return leads[0];
  }

  async updateLead(lead: Lead) {
    let leads = await this.getAllLeads() as Lead[];
    leads[0] = lead;
    await localStorage.setItem('leads', JSON.stringify(leads));
    console.log('lead-updated successfully!!')
  }
}
