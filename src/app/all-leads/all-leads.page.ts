import { Component, OnInit } from '@angular/core';
import { Lead } from '../model/lead/lead';
import { LeadDataService } from '../services/data/lead/lead-data.service';

@Component({
  selector: 'app-all-leads',
  templateUrl: './all-leads.page.html',
  styleUrls: ['./all-leads.page.scss'],
})
export class AllLeadsPage implements OnInit {

  leads:Lead[]=[]
  constructor(private leadDataService:LeadDataService) { }

  async ngOnInit() {
    this.leads = await this.leadDataService.getAllLeads();
  }
}
