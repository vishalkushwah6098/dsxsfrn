import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AllLeadsPageRoutingModule } from './all-leads-routing.module';

import { AllLeadsPage } from './all-leads.page';
import { AllLeadsCardComponent } from './all-leads-card/all-leads-card.component';
import { LeadDetailComponent } from '../lead/lead-detail/lead-detail.component';
import { AllLeadsHeaderComponent } from './all-leads-header/all-leads-header.component';
import { FiltersComponent } from '../filters/filters.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AllLeadsPageRoutingModule
  ],
  declarations: [
    AllLeadsPage,
    AllLeadsCardComponent,
    LeadDetailComponent,
    AllLeadsHeaderComponent,
    FiltersComponent
  ]
})
export class AllLeadsPageModule { }
