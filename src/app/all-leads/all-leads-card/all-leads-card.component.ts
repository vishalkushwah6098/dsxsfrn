import { Component, Input, OnInit } from '@angular/core';
import { Lead } from 'src/app/model/lead/lead';

@Component({
  selector: 'app-all-leads-card',
  templateUrl: './all-leads-card.component.html',
  styleUrls: ['./all-leads-card.component.scss'],
})
export class AllLeadsCardComponent implements OnInit {
  @Input() lead:Lead
  constructor() { }

  ngOnInit() {}

}
